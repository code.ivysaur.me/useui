# useui

![](https://img.shields.io/badge/written%20in-PHP-blue)

A multi-user web interface for SABnzbd+.

Original thread: http://forums.sabnzbd.org/viewtopic.php?f=6&t=8793


## Download

- [⬇️ useui-r1.7z](dist-archive/useui-r1.7z) *(10.94 KiB)*
